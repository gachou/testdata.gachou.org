---
home: true
heroImage: ./images/gachou-logo.png
footer: MIT Licensed | Copyright © 2018 - Nils Knappmeier
---

This site containes images and videos for testing [gachō, your picture album](https://gachou.org). The data of this server can be obtained through the [@gachou/testdata](https://www.npmjs.com/package/@gachou/testdata) 
module.


