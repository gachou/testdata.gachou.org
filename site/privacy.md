# Cookies

This site does not use cookies

# Data tracking

This site does not track you personally.
It does keep a protocol of incoming requests, but 
that protocol does not contain any data that could
personally identify you.