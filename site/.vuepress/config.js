module.exports = {
  title: '画帳 - gachō testdata',
  repo: 'https://gitlab.com/gachou/testdata.gachou.org',
  description: 'images and videos',
  head: [
    ['link', {rel: 'icon', href: '/favicon.ico'}]
  ],
  themeConfig: {
    nav: [
      {text: 'Home', link: '/'},
      {text: 'Impress', link: '/impress.html'},
      {text: 'Privacy', link: '/privacy.html'},
      {text: 'The gachō project', link: 'https://gachou.org'},
    ],
    search: false,
  }
}