#!/usr/bin/env bash


#####################
# GENERATE SSH KEYS #
#####################
function generateKey() {
    ALGO=$1
    if [ ! -f /config/ssh/ssh_host_${ALGO}_key ] ; then
        ssh-keygen -f /config/ssh/ssh_host_${ALGO}_key -N '' -t ${ALGO}
    fi
    chmod 700 /config/ssh/ssh_host_${ALGO}_key
}

mkdir -p /config/ssh/
generateKey rsa
generateKey dsa
generateKey ecdsa
generateKey ed25519

chown -R uploader /gachou-testdata
touch /config/ssh/authorized_keys_uploader
chmod 700 /config/ssh/authorized_keys_uploader
chown uploader.uploader /config/ssh/authorized_keys_uploader

exec "$@"

