# Rsync-Server for Gachou-Testdata

This server consists of three components

* An ssh-daemon for uploading media files via rsync
* An rsync-daemon for public downloads
* A web-server for showing a simple explanation page.

# SSH / Rsync

The data belongs to the user `gachou-testdata` and is stored
in the directory `gachou-testdata`.

